!-----------------------------------------------------------------------
!
!    Copyright 2018-2021
!    Kerry Key
!    Lamont-Doherty Earth Observatory, Columbia University
!    kkey@ldeo.columbia.edu
!
!    This file is part of MARE2DEM.
!
!    MARE2DEM is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    MARE2DEM is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with MARE2DEM.  If not, see <http://www.gnu.org/licenses/>.
!
!-----------------------------------------------------------------------

module kx_io

    use triangle_mesh
    
    implicit none      
    
    
!--------  
! Inputs:
!--------
    integer,public :: myID  

!
! Resistivity parameters:  The code uses a lookup table for both fixed and free resistivity parameters.
!
! For cAnisotropy:  
!
!  'isotropic':      rhoParams(i)         is rho  for mesh%attr = i
!  'triaxial':       rhoParams(3*(i-1)+1) is rhox for mesh%attr = i
!                    rhoParams(3*(i-1)+2) is rhoy for mesh%attr = i
!                    rhoParams(3*(i-1)+3) is rhoz for mesh%attr = i   
! 'tix','tiy','tiz': 
!                    rhoParams(2*(i-1)+1) is rho in symmetry axis (rhox for tix, rhoy for ti rhoz for tiz) for mesh%attr = i, 
!                    rhoParams(2*(i-1)+2) is rho in transverse plane (rhoyz for tix,rhoyz for tiy, rhoxy for tiz) for mesh%attr = i
! 'tiz_ratio':
!                    rhoParams(2*(i-1)+1) is rho z for mesh%attr = i, 
!                    rhoParams(2*(i-1)+2) is rho z/h ratio for mesh%attr = i
! 'isotropic_ip' Cole-Cole model:
!                    rhoParams(4*(i-1)+1) is rho0 for mesh%attr = i
!                    rhoParams(4*(i-1)+2) is eta  for mesh%attr = i
!                    rhoParams(4*(i-1)+3) is tau  for mesh%attr = i  
!                    rhoParams(4*(i-1)+4) is c    for mesh%attr = i 
! 'isotropic_complex' IP:
!                    rhoParams(4*(i-1)+1) is rho real for mesh%attr = i
!                    rhoParams(4*(i-1)+2) is rho imag for mesh%attr = i
 
           
    character(24), public                       :: cAnisotropy  = 'isotropic'  !'isotropic','triaxial','tix','tiy','tiz','tiz_ratio','isotropic_ip','isotropic_complex' 
    integer, public                             :: nrhoParams = 0    ! # of resistivities in rhoParams (=nRegions*nRhoPerRegion)
    real(8), dimension(:), allocatable, public  :: rhoParams         ! array of conductivities for for fixed and free parameters    
    integer, dimension(:), allocatable, public  :: iFreeParam        ! Integer of free parameter # for each value in rhoParams

!
! Transmitter parameters:
!
    real(8), public :: kx   ! 2.5D Fourier transformation wavenumber, ignored for MT computations
    real(8), public :: w    ! Angular frequency 
    integer, public :: nTx  ! set to 1 for MT
    real(8), dimension(:), allocatable, public :: xTx, yTx, zTx, azimuthTx,dipTx, lengthTx ! x,y,z location of the transmitter 
    logical, dimension(:), allocatable, public :: lNeg_ikx ! Set to true for neg ikx transmitters.
    character(8), allocatable, public          :: TxType(:)  !'edipole' or 'bdipole',    

    character(2), public :: sType   ! 'cs','mt', or 'dc'

!
! Receiver parameters:
!
    integer, public :: nRx                                     ! Number of receivers       
    real(8), dimension(:), allocatable, public :: xRx, yRx, zRx, azimuthRx,dipRx, lengthRx ! x,y,z location of the transmitter 
    character(8), allocatable, public          :: RxType(:)  !'edipole' or 'bdipole',   
    
    
!
! DC resistivity Rx and Tx parameters:
! 
    integer, public :: ntrodes_tx, ntrodes_rx
    real(8), dimension(:,:), allocatable, public ::  electrode_tx,electrode_rx ! x,y,z positions of electrodes for Tx and Rx.
                                           
!
! Quadrature weights used for finite dipole integrations:
!
    integer, public                     :: nquad_Tx = 0, nquad_Rx = 0

!
! Data masks. Set to true if EM fields to be computed at particular Rx-Tx pair.
!
    integer, dimension(:,:), allocatable, public :: iDataMask ! 0 = no data, 1 = no data but include for refinement, 2=data present
    logical :: l_has_TM_mode = .false. 
    logical :: l_has_TE_mode = .false.
    
!
! Mesh parameters are stored in the derived type 'trimesh' for ease of use
!
    type (trimesh), public :: mesh    
    character(256), public :: fileroot         ! base name for mesh files 
    integer, public        :: meshnumber       ! keeps track of mesh refinement number (1 = starting mesh)
    
!
! Local a priori refinement parameters:
!
    logical, public :: lLocalRefine  = .true.        ! Set to true to refine grid on input using skin depth rules
    real(8), public :: nTxEleArea = 1d4, nRxEleArea = 1d4 ! Absolute tolerance for a priori refinement around Tx's and Rx's
    real(8), public :: nFracSkinDepthsRx = 0.05  ! elements containing Rx's refined until diameter < skin_depth*nFracSkinDepthsRx
    real(8), public :: nFracSkinDepthsRegional = 0.1 ! 0.3 ! elements within 1 skin depth of Rx's refined until 
                                                           ! diameter < skin_depth*nFracSkinDepthsRegional
                                                           ! generally nFracSkinDepthsRx should be smaller than nFracSkinDepthsRegional
    real(8), public :: nTxEleAreaDC = 1d-2, nRxEleAreaDC = 1d-2
                                                                                                                      
!
!  Adjustable parameters controlling the adaptive refinement and error estimation:
!
    real(8), public :: errortolerance    = 1.0       ! tolerance for relative error
    real(8), public :: minRangeProtector = 25.       ! (m) protects agains over refinement close to the Tx
    real(8), public :: rxCloud           = 0d0       ! radius for element cloud around Rx to use for dual function subdomain. 
    real(8), public :: minArea           = 1d-4      ! minimum area to stop refinement from making super tiny elements  

    real(8), public :: pct_refine_elements = 5.      ! minimum percent of worst elements to refine   
    real(8), public :: pct_refine_error    = 35.     ! percent of total error for selecting to refine  
    real(8), public :: refinement_area_divisor = 4.  ! divide element area by this number to get target area when refining with triangle.c
    real(8), public :: ecutoff           = 1d-17     ! E(kx) error floor in V/m/Am  
    real(8), public :: hcutoff           = 1d-17     ! H(kx) error floor A/m/Am, note  B = 1.26x10^-6 * H
    integer, public :: minRefinements    = 1         ! minimum number of refinements, to ensure accurate solution...  
    integer, public :: maxRefinements    = 30        ! stop runaway refinements...    
    integer, public :: maxMeshNodes      = 200000    ! max# nodes in mesh, stop refinement if mesh grows this big. This will keep 
                                                     ! the code from running out of memory, but response accuracy may be compromised                        
!
! Model response derivatives:
!    
    logical, public :: lCompDerivs ! set =.true. to output the field derivatives with respect to conductivity
    
!
! Other adjustable parameters:
!
    real(8), public         :: minqangle          = 25.    ! quality angle for triangle.c (minimun inner angle for triangulation)
    character(32), public   :: linearSolver       = 'intelmkl'    ! linear solver for FE systems: 'superlu' or 'intelmkl'
    logical, public         :: lprintDebug_em2dkx = .false.
    logical, public         :: lprintDebug_dc2dkx = .false.
    logical, public         :: lDisplayRefinementStats = .true.   
    logical, public         :: lSaveLinearSystem  = .false. ! write out linear system Ax=b to files A = *.lhs, b=*.rhs, x=*.exhx
    logical, public         :: lSaveMeshFiles     = .false. ! set to .true. to write out the mesh to .poly, .ele and .node files   
    integer, public         :: idual_func         = 2       ! 0,1,2 for G0,G1,G2 error functionals from our paper
    logical, public         :: lSolveBumpGaussSeidel= .true.   
    logical, public         :: lMTdirichlet       = .true.  ! use Dirichlet boundary condition for MT, else uses Neumann BC
    logical, public         :: lMTscatteredField  = .false. ! set to true to use scattered field (better at high freq for deep ocean MT)
    logical, public         :: lUseReciprocity    = .false.   ! if true, this properly scales J to B or M to E transmissions by ommu
    
!-----------
! Outputs:
! ---------
 
! Requested responses for various components: 

    integer, public                                 :: nComponents !kwk debug: these two are Inputs not outputs
    integer, dimension(:,:), allocatable, public    :: Components  ! [nComps x 2] for iRx,iTx, Rx and Tx types determine E or B.
    complex(8), dimension(:), allocatable, public   :: Fields      ! nComps array of field response for each requested component 
    complex(8), dimension(:,:), allocatable, public :: dFieldsdRho  ! sensitivity for each requested field component if inversion
    
    public :: deallocate_kx_io
    
    contains
    
!-----------------------------------------------------------------------------------------------------------------------------------
    subroutine deallocate_kx_io

!
! Deallocate public i/o variables in this module
!    
    if ( allocated( rhoParams ) )       deallocate( rhoParams )   
    if ( allocated( iFreeParam ) )      deallocate( iFreeParam )      
    if ( allocated(xTx) )               deallocate ( xTx,yTx,zTx,azimuthTx,dipTx,TxType, lengthTx ) 
    if ( allocated(lNeg_ikx) )          deallocate ( lNeg_ikx ) 

    if ( allocated(xRx) )               deallocate ( xRx,yRx,zRx,azimuthRx,dipRx,RxType, lengthRx ) 

    if ( allocated(electrode_rx) )     deallocate( electrode_tx,electrode_rx )
    
    if (allocated(Fields))             deallocate(Fields)
    if (allocated(dFieldsdRho))        deallocate(dFieldsdRho)
    if (allocated(Components))         deallocate(Components)
    if (allocated(iDataMask))          deallocate(iDataMask)
 
    
    end subroutine deallocate_kx_io   
    
end module kx_io
 